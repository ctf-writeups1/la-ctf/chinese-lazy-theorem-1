# chinese-lazy-theorem-1        238 pt



## Description 

I heard about this cool theorem called the Chinese Remainder Theorem, but, uh... I'm feeling kinda tired right now.

nc lac.tf 31110

file : chinese-lazy-theorem-1.py

## Analyse

We have a tcp serv at lac.tf, port 31111.

Reading the python, or connecting to the server, we know to obtain the flag from the server, we have to guess a random generated number between 1 and the product of two primes numbers of 512 bit (We call it x).

At the begining of the connection, we receive the values for the two Big primes numbers. (a b)

New x and primes numbers are calculted for each new connections.

To guess x, we can ask to the server the euclidean remain of a number that we choose ourself.

There are not limitation about the value we can enter, except it have to be positiv value.

## Method

So here it's simple, we just have to ask for the result of a number bigger than x to obtain x.
As 0 < x < ab, we juste have to ask as modulo the result of ab to obtain x.
No need script, just connect with netcat, multiply primes, ask for the resulted x and pass x
